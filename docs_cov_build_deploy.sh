#!/bin/sh

# coverage: clean, build, deploy
rm -rf htmlcov
sudo rm -rf /var/www/html/PyOTRS/htmlcov
~/work/PyOTRS_project/venv/bin/coverage combine .coverage.data
~/work/PyOTRS_project/venv/bin/coverage html -d htmlcov
sudo cp -a htmlcov /var/www/html/PyOTRS/

# docs: clean, build, deploy
rm -rf build/docs
sudo rm -rf /var/www/html/PyOTRS/docs
tox -e docs
sudo cp -a build/docs /var/www/html/PyOTRS/docs

# check format (README)
python setup.py check --strict --metadata --restructuredtext
